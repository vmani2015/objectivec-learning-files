//
//  Employee.m
//  FirstProjectObjC
//
//  Created by Vinu Mani on 9/11/2016.
//  Copyright © 2016 VimanEnterprises. All rights reserved.
//

#import "Employee.h"

@implementation Employee

- (void) someMethod
{
  NSLog(@"This is the some method working!!!");
}

-(int)multiplyBy10:(int)input
{
  NSLog(@"The input is %i",input);
  NSLog(@"The result is %i",input*10);
  return input*10;
}

-(int)multipleSpecify:(int)input multiplier:(int)test
{
  NSLog(@"The input is %i",input);
  NSLog(@"The multiplier is %i",test);
  NSLog(@"The result is %i",input*test);
  return input* test;
}

-(NSString *)tolowercase:(NSString *)input
{
  return input.lowercaseString;
}

-(NSString *)touppercase:(NSString *)input
{
  return input.uppercaseString;
}

-(NSString *)concatinateString:(NSString *)input OtherString:(NSString *)input2
{
  
  NSString *new_string = [[NSString alloc] initWithFormat:@"%@,%@",input,input2];
  
  NSLog(@"The concatinated string is %@",new_string);
  return new_string;
}

@end
