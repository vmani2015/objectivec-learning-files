//
//  Employee.h
//  FirstProjectObjC
//
//  Created by Vinu Mani on 9/11/2016.
//  Copyright © 2016 VimanEnterprises. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Employee : NSObject

@property NSString *name;
@property int *emp_number;
@property NSDate *hire_date;


- (void) someMethod;

- (int) multiplyBy10:(int) input;

- (int) multipleSpecify:(int) input multiplier:(int) test;

-(NSString *) tolowercase:(NSString *) input;

-(NSString *) touppercase:(NSString *) input;

-(NSString *) concatinateString:(NSString *) input OtherString:(NSString *) input2;










@end
