//
//  Calculator.h
//  FirstProjectObjC
//
//  Created by Vinu Mani on 10/11/2016.
//  Copyright © 2016 VimanEnterprises. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Calculator : NSObject

- (int) addNumber:(int) number1 addNumber2:(int) number2;

- (int) subtractNumber:(int) number1 subtractNumber2:(int) number2;

- (int) divideNumber:(int) number1 divideNumber2:(int) number2;

- (int) multiplyNumber:(int) number1 multiplyNumber2:(int) number2;



@end
