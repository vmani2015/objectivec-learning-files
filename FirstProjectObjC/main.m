//
//  main.m
//  FirstProjectObjC
//
//  Created by Vinu Mani on 29/10/2016.
//  Copyright © 2016 VimanEnterprises. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Employee.h"
#import "Calculator.h"

void printString(NSString* string)
{
  NSLog(@"The string is : %@",string);
}


void printEvenNumbers(BOOL option,int number)
{
  
  (option == TRUE)? NSLog(@"The option selected is TRUE") : NSLog(@"The option selected is FALSE");
  NSLog(@"The number inputted is %i",number);
  
  if ( option == true)
    {
    
    //Loop to print out all even numbers
    for ( int j = 0 ; j < number ; j++)
      {
      if ( j % 2 != 0)
        {
        continue;
        }
      NSLog(@"The current number is %i",j);
      }
    }
  
  
  else
    {
    
    //Loop to print out all odd numbers
    
    for ( int j = 0 ; j < number ; j++)
      {
      if ( j % 2 == 0)
        {
        continue;
        }
      NSLog(@"The current number is %i",j);
      }
}

}

int main(int argc, const char * argv[]) {
  @autoreleasepool {
      // insert code here...
      NSLog(@"Hello, World!");
    
    int years = 5;
    int months = 12;
    int days = 30;
    
    int total = years * months * days;
    
    
    
    if (years >1)
    {
    NSLog(@"There are %i total days in %i years",total,years);
    }
    else
    {
    NSLog(@"There are %i total days in %i year",total,years);
    }
    
    switch (years) {
      case 1:
        NSLog(@"This is 1 year");
        break;
        case 2:
        case 5:
        case 10:
        NSLog(@"This is 5 years");
        break;
      default:
        NSLog(@"This is more stuff");
        break;
    }
    
    //Ternary and Modulus example
    
    int year = 2006;
    
    //Ternary syntax
    // (condition) ? true : false ;
    
    (year % 4 == 0)? NSLog(@"This is a leap year") : NSLog(@"This is not a leap year");
    
//    if (year % 4 == 0)
//      {
//      NSLog(@"This is a leap year");
//      }
//    else
//      {
//      NSLog(@"This is not a leap year");
//      }
    
    //Loops
    //While loop
    
    int a = 1;
    
    while ( a < 10)
      {
      NSLog(@"The value of a is %i",a);
      a++;
      
      if ( a == 5)
        {
        break;
        }
      
      }
    
    //For loop
    
    for ( int i = 0 ; i < 10 ; i++)
      {
      NSLog(@"The value of i is %i",i);
      }
    
    
    //Trying my own function
    printEvenNumbers(YES, 30);
    
    //Declaring my own String
    NSString *test = @"Hello";
    
    //Passing it to a function
    
    printString(test);
    
    //Wroknig with functions and methods in Objective C
    NSString *message = @"This is the second String";
    
    NSString *uppercase = [message uppercaseString];
    
    NSLog(@"The Upper Case String is %@",uppercase);
    
    NSLog(@"The Lower Case String is %@",[message lowercaseString]);
    
    NSDate *new_date = [NSDate date];
    
    NSLog(@"The Current Date is %@",new_date);
    
    //Memory Management
    
    NSString *new_string = [[NSString alloc] initWithFormat:@"Test"];
    
    NSLog(@"The string is %@",[[NSString alloc]initWithFormat:@"Cool"]);
    
    
    
    
    //My Own Class
    
    Employee *fred = [[Employee alloc] init];
    
    [fred someMethod];
    
    [fred multiplyBy10:20];
    
    [fred multipleSpecify:6 multiplier:304];
    
    [fred concatinateString:new_string OtherString:message];
    
    
    //My Own other Call
    
    Calculator *calc = [[Calculator alloc] init];
    
    int i = 10;
    int j = 20;
    
    double res = [calc addNumber:i addNumber2:j];
    
    NSLog(@"The result of the addition is %D",res);
    
    res = [calc subtractNumber:i subtractNumber2:j];
    
    NSLog(@"The result of the subtraction is %f",res);
    
    res = [calc divideNumber:i divideNumber2:j ];
    NSLog(@"The result of the division is %f",res);
    
    res = [calc multiplyNumber:i multiplyNumber2:j];
    NSLog(@"The result of the multiplication is %f",res);
    
    NSLog(@"This line was written after the first commit");
    
    
    
    
    
    
    
    
    
 
    
    
    
    
  }
    return 0;
}
